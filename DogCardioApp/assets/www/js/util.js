 $(document).on('expand', '.ui-collapsible', function(event) {
     var contentDiv = $(this).children('.ui-collapsible-content');
     contentDiv.hide();
     contentDiv.slideDown(300);
     event.stopPropagation(); // don't bubble up
 })

 $(document).on('collapse', '.ui-collapsible', function(event) {
         var contentDiv = $(this).children('.ui-collapsible-content');
         contentDiv.slideUp(300);
     event.stopPropagation(); // don't bubble up
 });

 /********** Funciones redireccion *********/
 
function goMenu () {
	muestraDialogo("&iquest;Desea abandonar el cuestionario?", "a");
}

function redirectMenu () {
	clearDataVars("respReg");
	redirect('menu.html');	
}

function initQuest () {
	localStorage.setItem("pageAnt", "question");
	clearDataVars("respReg");
	redirect('quest1.html');
}

function redirect(pagRedirect, effect){
	if (!effect) {
		effect = "fade";		
	}
	$.mobile.changePage(pagRedirect,{
	      changeHash:true,
	      reloadPage:true,
	      transition: effect
	      });	
}

function initMediciones () {
	clearDataVars("respMed");
	redirect('questMeasur.html');
}

function goToResult (pageDesde){
	localStorage.setItem("pageAnt", pageDesde);
	redirect("result.html");
}

/************************************************/
/***** funciones gestion localStorage ******/

function saveValuesConcat (nameVar, valueVar) {
	var valueVarAnt = "";
	var exist = false;
	if(getVar(nameVar)){
		//comprobamos si ya existe
		if (getVar(nameVar).indexOf(valueVar) != -1) {
			exist = true;
		} else {
			valueVarAnt = getVar(nameVar) + "|";
		}
	}

	if (!exist) {
		var totValueVar = valueVarAnt + valueVar;
		localStorage.setItem(nameVar, totValueVar);	
	}
}
 
 function removeValueConcat (nameVar, valueDelete) {
	if (getVar(nameVar) && 
		getVar(nameVar).indexOf(valueDelete) != -1) {
		var newValVar = getVar(nameVar).replace(valueDelete, "");
		localStorage.setItem(nameVar, newValVar);
	}
} 

function clearDataVars (nameVar) {
	if (getVar(nameVar)) {
		var arrRespReg = getVar(nameVar).split("|");
		for (var i in arrRespReg) {
			if (arrRespReg[i] != "" && localStorage.getItem(arrRespReg[i])) {
				localStorage.removeItem(arrRespReg[i]);
			}
		}
		localStorage.removeItem(nameVar);
	}
}

/*********************************************/
/********** Funciones Alert Dialog *********/

function muestraDialogo(msg, theme) {
    $.dynamic_popup({
        content: '<p>'+msg+'</p>',
        btnAcceptLabel: 'Si',
        btnCancelLabel: 'No',
        dialog: true,
        themeMain: theme,
        callFunctionAccept: true
    })

}

function muestraAlert(msg, theme) {
	$.dynamic_popup({
        content: '<p>'+msg+'</p>',
        btnAcceptLabel: 'Aceptar',
        dialog: false,
        themeMain: theme,
        callFunctionAccept: false
    })
}

/********************************************/

function getNumQuest (idPag){
	//numero de preguntas = numero de fieldset
//	return document["form_"+idPag].getElementsByName("fieldsetPreg").length;
	return document.getElementsByName("fieldsetPreg").length;
}

/**
 * Limpiamos las respuestas almacenadas de procesos anteriores
 * y mantenemos el contador de diagnosticos realizados
 */
function clearStorage (){
	var contDiags = getVar("numDiags");
	if (contDiags == null || contDiags == "null" || isNaN(contDiags)) {
		contDiags = 0;
	}
	//Borramos localStorage
	localStorage.clear();
	//Almacenamos el numero de diagnosticos realizados
	localStorage.setItem("numDiags",contDiags);	
}

function getVar (nomVar){
	return localStorage.getItem(nomVar);
}

function getElem (idElem) {
	return document.getElementById(idElem);
}