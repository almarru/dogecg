function nextQuest (idPag){
	//1. Almacenamos las respuestas
	var optionsSel = saveResp(idPag);
	//Comprobamos si ha respondido a todas las preguntas
	if (!optionsSel) {
		muestraAlert("Debe responder a todas las preguntas", "a");
		return;
	}
	
	//2. Cargamos la siguiente pantalla
	var pagRedirect = getNextQuest(idPag);
	redirect(pagRedirect, "slide");
}

function saveResp (idPag) {
	var optionsPreg;
	var respSel;
	var numPreg = getNumQuest(idPag);
	var optionsSel = true;
	for (var i=1; i<=numPreg; i++) {
		optionsPreg = document.getElementsByName("resp_"+idPag+"_"+i);	
		if (optionsPreg != null && optionsPreg.length > 0) {
			optionsSel = false;
			for (var j=0; j<optionsPreg.length; j++) {
				if (optionsPreg[j].checked) {
					//Almacenamos la respuesta a la pregunta
					localStorage.setItem("res_"+idPag+"_"+i, optionsPreg[j].value);
					//Almacenamos el identificador de la respuesta
					saveValuesConcat("respReg", "res_"+idPag+"_"+i);
					optionsSel = true;
				}
			}
			if (!optionsSel) {
				break;
			}
		}
	}
	return optionsSel;
}

function getNextQuest (idPag){
	//Pregunta 1
	if (idPag == "preg1") {
		if (getVar("res_preg1_1") == "si") {
			//Respuesta de la pregunta 1 = SI -> realizar las preguntas de 1a
			return "quest1a.html";
		} else{
			//Respuesta de la pregunta 1 = NO -> realizar pregunta 2
			return "quest2.html";
		}
	}

	//Pregunta 1a
	if (idPag == "preg1a") {
		if (getVar("res_preg1a_3") == "si") {
			//Respuesta de la 3� pregunta de 1a = SI -> realizar la pregunta de 1b
			return "quest1b.html";
		} else{
			//Respuesta de la 3� pregunta de 1a = NO -> realizar pregunta 2
			return "quest2.html";
		}
	}	
	
	//Pregunta 1b
	if (idPag == "preg1b"){
		return "quest2.html";
	}	
	
	//Pregunta 2
	if (idPag == "preg2") {
		if (getVar("res_preg2_1") == "si") {
			//Respuesta de la pregunta 2 = SI -> realizar las preguntas de 2a
			return "quest2a.html";
		} else {
			//Respuesta de la pregunta 2 = NO (NO REALIZAR PREGUNTA 3) -> realizar pregunta 4
			//indicar respuesta a la pregunta 3 NO
			localStorage.setItem("res_preg3_1", "no");
			return "quest4.html";
		}
	}
	
	//Pregunta 2a
	if (idPag == "preg2a"){
		if (getVar("res_preg2a_2") == "si"){
			return "quest3.html";		
		} else {
			return "quest2b.html";		
		}
	}

	//Pregunta 2b
	if (idPag == "preg2b"){
		return "quest3.html";
	}	
	
	//Pregunta 3
	if (idPag == "preg3") {
		return "quest4.html";
	}

	//Pregunta 4
	if (idPag == "preg4") {
		if (getVar("res_preg2_1") == "si"){
			//Respuesta de la pregunta 2 = SI..
			if (getVar("res_preg2a_1") == "no") {
				//Si la respuesta a la pregunta 2 = SI y 2a = NO  -> realizar la pregunta 5a
				return "quest5a.html";
				
			} else if (getVar("res_preg2a_1") == "si") {
				//Si la respuesta a la pregunta 2 = SI y 2a = SI -> realizar la pregunta 5b
				return "quest5b.html";
			}			
		} else {
			//Si la respuesta a la pregunta 2 = NO -> No se realiza la pregunta 5 ni la 6
			return "result.html";
		}
	}
	
	//Pregunta 5a o 5b
	if (idPag == "preg5a" || idPag == "preg5b"){
		if (resp2abSi() && getVar("res_preg3_1") == "si" && getVar("res_preg4_1") != "alta") {
			//Si respuesta 2 = si Y 2a = si Y 2b = SI Y Si la respuesta 4 no es "alta" o "muy alta"
			return "quest6.html";    				
		} else {
			return "result.html";			
		}
	}

	//Pregunta 6
	if (idPag == "preg6"){
		return "result.html";
	}

}

function resetResp (idPag){
	var numPreg = getNumQuest(idPag);
	for (var i=1; i<=numPreg; i++) {
		localStorage.removeItem("res_"+idPag+"_"+i);
		removeValueConcat("res_"+idPag+"_"+i);
	}
}

function getDiags () {
	var diags = new Array();

	//TAQUICARDIA VENTRICULAR - FIBRILACION ATRIAL
	diags = getDiagTaquicardiaVent_FibrilacionAtrial (diags);	

	//BLOQUEOS AV
	diags = getDiagBloqueoAV (diags);
	
	//SINUSAL - RITMO DE ESCAPE
	diags = getDiagsSinusal_RitmoEsc (diags);
	
	//FLUTTER ATRIAL
	diags = getDiagFlutterAtrial (diags);
	
	//CPV
	diags = getDiagCPV (diags);
	
	//BLOQUEOS EN RAMA
	diags = getDiagBloqueoRama (diags);
	
	//COMPLEJOS PREMATUROS SUPRAVENTRICULARES - TAQUICARDIA SUPRAVENTRICULAR
	diags = getDiagComplejosPrematurosSupraVentr (diags);
	
	//BLOQUEO/PARO SINUSAL INCOMPLETO
	diags = getDiagBloqueoSinusalIncompleto(diags);
	
	//SECUESTRO SINUSAL
	diags = getDiagSecuestroSinusal(diags);
	
	return diags;
	
}

/**
 * Funcion que comprueba si existe diagnostico de taquicardia ventricular o
 * fibrilacion atrial y lo anyade al array de diagnosticos 
 * @param diags
 * @returns array - diags
 */
function getDiagTaquicardiaVent_FibrilacionAtrial (diags) {
	//Preg1 = si && pre1a_1 = no && preg1a_2 = si && preg4 = normal o alta
	if (getVar("res_preg1_1") == "si" && getVar("res_preg1a_1") == "no" &&
		getVar("res_preg1a_2") == "si" && (getVar("res_preg4_1") == "normal" || getVar("res_preg4_1") == "alta")) {
		if (getVar("res_preg1a_3") == "si") {
			//preg1a_3 = si
			var taqVentr = "Taquicardia ventricular con";
			
			if (getVar("res_preg1b_1") == "positiva") {
				//res_preg1b_1 = positiva
				diags.push(taqVentr+" origen en ventr&iacute;culo derecho");
				
			} else if (getVar("res_preg1b_1") == "negativa"){
				//res_pre1b_1 = negativa
				diags.push(taqVentr+" origen en ventr&iacute;culo izquierdo");			
			}
			
		} else if (getVar("res_preg1a_3") == "no") {
			//preg1a_3 = no
			diags.push("Taquicardia ventricular con distintos or&iacute;genes");
			
		}		
	} else {
		if (getVar("res_preg2_1") == "no" && getVar("res_preg4_1") == "alta") {
			diags.push("Fibrilaci&oacute;n atrial");
		}
	}
	return diags;
}

/**
 * Funcion que comprueba si existen diagnosticos relacionados con
 * sinusal o ritmo de escape los anyade al array de diagnosticos
 * @param diags
 * @returns array - diags
 */
function getDiagsSinusal_RitmoEsc (diags) {
	if (getVar("res_preg3_1") == "si" && resp2abSi()){
		//SINUSAL
		//Preg1 = si && todas Preg2 deben de haberse realizado = si
		diags = getDiagsSinusal(diags);
	} else {
		//RITMOS DE ESCAPE
		//Si no se dan las condiciones para un ritmo sinusal
		diags = getDiagRitmoEsc(diags);
	}
	return diags;
}

/**
 * Funcion que comprueba si existe diagnostico sinusal
 * y los anyade al array de diagnosticos
 * @param diags
 * @returns array - diags
 */
function getDiagsSinusal (diags) {
	//No puede haber Taquicardia Ventricular
	if (!diagsContieneNomDiag(diags, "Taquicardia Ventricular")) {
		diags.push("Ritmo de base sinusal");
		//Comprobar Preg4
		if (getVar("res_preg4_1") == "alta"){
			//Preg4 = alta
			diags.push("Taquicardia sinusal");
		} 
	}
	if (resp4BajaOMuyBaja()) {
		//Preg4 = baja o muybaja
		diags.push("Bradicardia sinusal");
	}
	return diags;
	
}

/**
 * Funcion que comprueba si existe diagnostico relacionado con
 * el ritmo de escape y lo anyade al array de diagnosticos
 * @param diags
 * @returns array - diags
 */
function getDiagRitmoEsc (diags) {
	var descDiag = "";
	if (getVar("res_preg2_1") == "no" || 
		diagsContieneNomDiag(diags, "Bloqueos AV de 3&ordm; grado")) {
		
		if (getVar("res_preg1_1") == "si" && getVar("res_preg4_1") == "muybaja"){
			//Preg1 = si && Preg4 = muybaja
			descDiag += "Ritmo de escape de origen ventricular";
			
		} else if (getVar("res_preg2a_2") == "no" && resp4BajaOMuyBaja()) {
			//Preg2a_2 = no && (Preg4 = baja o muybaja)
			descDiag += "Ritmo de escape de origen AV";			
		
		}
		
		//Se ha detectado un ritmo de escape y se a�ade al array de diagnosticos 
		if (descDiag != ""){
			diags.push(descDiag);			
		}
	}
	return diags;
}

/**
 * Funcion que comprueba si existe diagnostico relacionado con
 * bloqueo AV y lo anyade al array de diagnosticos 
 * @param diags
 * @returns array - diags
 */
function getDiagBloqueoAV (diags) {
	//Siempre se diganosticara el bloqueo de mayor grado
	if (getVar("res_preg2_1") == "si" && getVar("res_preg2a_1") == "no" &&
		getVar("res_preg5a_1") == "1" && resp4BajaOMuyBaja()) {
		  //Preg2 = si && preg2a_1 = si && Preg5 = 1 && Pre4 = baja o muybaja
		diags.push("Bloqueos AV de 3&ordm; grado");	
		
	} else if (getVar("res_preg2_1") == "si" && getVar("res_preg2a_1") == "no" &&
			   getVar("res_preg3_1") == "si") {
		  //Preg2 = si && preg2a_1 = no && Preg3 = si
		if (getVar("res_preg5a_1") == "3") {
			//Preg5 = 3
			diags.push("Bloqueos AV 2&ordm; grado Mobitz tipo I");
			
		} else if (getVar("res_preg5a_1") == "2") {
			//Preg5 = 2
			diags.push("Bloqueos AV 2&ordm; grado Mobitz tipo II");
			
		}
	} else if (getVar("res_preg2_1") == "si" && getVar("res_preg2a_1") == "si" &&
			   getVar("res_preg5b_1") == "largo") {
		  //Preg2 = si && preg2a_1 = si && Preg5 = largo
		  diags.push("Bloqueos AV 1&ordm; grado");		
	}

	return diags;
}

/**
 * Funcion que comprueba si existe diagnostico Flutter Atrial
 * y lo anyade al array de diagnosticos 
 * @param diags
 * @returns array - diags
 */
function getDiagFlutterAtrial (diags) {
	//Preg1 = no && preg2a_1 = no && preg4 = alta && preg5 = 4
	if (getVar("res_preg1_1") == "si" && getVar("res_preg2a_1") == "no" && 
		getVar("res_preg4_1") == "alta" && getVar("res_preg5a_1") == "4") {
		 diags.push("Flutter atrial");				
	}
	return diags;
}

/**
 * Funcion que comprueba si existe diagnostico CPV
 * y lo anyade al array de diagnosticos 
 * @param diags
 * @returns array - diags
 */
function getDiagCPV (diags) {
	//Preg1 = si && preg1a_1 = no && preg1a_2 = no
	if (getVar("res_preg1_1") == "si" && getVar("res_preg1a_1") == "no" &&
		getVar("res_preg1a_2") == "no"){
		
		if (getVar("res_preg1a_3") == "si") {
			//preg1a_3 = si
			
			if (getVar("res_preg1b_1") == "positiva") {
				//res_preg1b_1 = positiva
				diags.push("CPVs con origen en ventr&iacute;culo derecho");
				
			} else if (getVar("res_preg1b_1") == "negativa"){
				//res_preg1b_1 = negativa
				diags.push("CPVs con origen en ventr&iacute;culo izquierdo");			
			}
			
		} else if (getVar("res_preg1a_3") == "no") {
			//preg1a_3 = no
			diags.push("CPVs con distintos or&iacute;genes");
			
		}
	}
	return diags;
}

/**
 * Funcion que comprueba si existe diagnostico de bloqueo en rama
 * y lo anyade al array de diagnosticos 
 * @param diags
 * @returns array - diags
 */
function getDiagBloqueoRama (diags) {
	
	//preg1 = si && preg1a_1 = si && preg1a_3 = si
	//No puede haber Bloqueo AV de 3� grado
	//No puede haber Ritmo de escape
	if (getVar("res_preg1_1") == "si" && getVar("res_preg1a_1") == "si" &&
		getVar("res_preg1a_3") == "si" &&
		!diagsContieneNomDiag(diags, "Bloqueos AV de 3&ordm; grado") && 
		!diagsContieneNomDiag(diags, "Ritmo de escape")){
		if (getVar("res_preg1b_1") == "positiva") {
			//res_preg1b_1 = positiva
			diags.push("Bloqueo de rama izquierda");
		
		} else if (getVar("res_preg1b_1") == "negativa") {
			//res_preg1b_1 = negativa
			diags.push("Bloqueo de rama derecha");				
		}		
		
	}

	return diags;
}

/**
 * Funcion que comprueba si existe diagnostico Complejos Prematuros supraventriculares
 * o Taquicardia supraventricular y lo anyade al array de diagnosticos 
 * @param diags
 * @returns array - diags
 */
function getDiagComplejosPrematurosSupraVentr (diags) {
	if (getVar("res_preg2_1") == "si" && ((getVar("res_preg2a_2") == "no" && 
		 getVar("res_preg2b_1") == "no") || getVar("res_preg3_1") == "no")){
		diags.push("Complejos Prematuros supraventriculares");
	
	} 
	if (getVar("res_preg2_1") == "si" && getVar("res_preg2a_2") == "no" && 
		getVar("res_preg2b_1") == "si") {
		diags.push("Taquicardia supraventricular");
		
	}
	return diags;
}

/**
 * Funcion que comprueba si existe diagnostico bloqueo/paro 
 * sinusal incompleto y lo anyade al array de diagnosticos 
 * @param diags
 * @returns array - diags
 */
function getDiagBloqueoSinusalIncompleto(diags) {
	if (getVar("res_preg6_1") == "si") {
		//res_preg6_1 = si
		diags.push("Bloqueo/paro sinusal incompleto");			
	}
	return diags;
}

/**
 * Funcion que comprueba si existe diagnostico secuestro 
 * sinusal y lo anyade al array de diagnosticos 
 * @param diags
 * @returns array - diags
 */
function getDiagSecuestroSinusal(diags) {
	if (getVar("res_preg2_1") == "no" && diagsContieneNomDiag(diags, "Ritmo de escape")) {
		//res_preg2_1 = si && debe haber un ritmo de escape
		diags.push("Secuestro sinusal");
	}
	return diags;
}

function resp4BajaOMuyBaja () {
	if (getVar("res_preg4_1") == "baja" || getVar("res_preg4_1") == "muybaja") {
		return true;
	} else {
		return false;
	}
}

function resp2abSi () {
	if (getVar("res_preg2_1") == "si" && getVar("res_preg2a_1") == "si" &&
		getVar("res_preg2a_2") == "si"){
			return true;
		} else {
			return false;
		}	
}

function resp2TotalSi (){
	if (resp2abSi() && getVar("res_preg2b_1") == "si"){
		return true;
	} else {
		return false;
	}
}

function getResultDiag () {
	var diags = getDiags();
	var strDiags = "";
	
	for (var i in diags) {
		strDiags += "<br>" + diags[i];
	}
	
	return strDiags;
}

function diagsContieneNomDiag (diags, nomDiag) {
	for (var i in diags) {
		if (diags[i] != null && (diags[i].toLowerCase().indexOf (nomDiag.toLowerCase()) != -1)){
			return true;
		}
	}	
	return false;
}

function showDiags () {
	var diags = getDiags();
	var contentShow = "";
	
	if (diags != null && diags.length > 0) {
		var newSection = "<div data-role='collapsible' data-theme='c' data-content-theme='c'>";
		var contentSec;
		for (var i in diags) {
			if (diags[i] != "") {
				contentSec = newSection;
				contentSec += "<h3>"+diags[i]+"</h3>";
				contentSec += "<p>Explicaci&oacute;n Diagn&oacute;stico</p>";
				contentSec += "</div>";
				contentShow += contentSec;
			}
		}
	} else {
		if (getVar("pageAnt") == "question") {
			contentShow = "<p>No se han obtenido Diagn&oacute;sticos.</p>";
		} else {
			contentShow = "<p>Todav&iacute;a no se han registrado Diagn&oacute;sticos.</p>";
		}		
	}
	$(".csColl").append(contentShow).trigger('create');
}

function showMed () {
	if (getVar("respMed")) {
		var arrMeds = getVar("respMed").split("|");
		var contentShow ="";
		var nameVarMed = "";
		var nameMed = "";
		var valueMed = "";
		
		for (var i in arrMeds) {
			if (arrMeds[i] != "") {
				nameVarMed = arrMeds[i];
				valueMed = localStorage.getItem(nameVarMed);
				
				if (nameVarMed.indexOf("dur") == 0) {
					//Duracion
					nameMed = getNameDuracion(nameVarMed);
					valueMed += " s";
				} else if (nameVarMed.indexOf("int") == 0) {
					//Intensidad
					nameMed = getNameIntensidad(nameVarMed);
					valueMed += " mV";
					
				}				
				contentShow += "<p><b>"+nameMed+":</b> "+valueMed+"</p>";
			}
		}
	} else {
		contentShow = "<p>Todav&iacute;a no se han registrado las medidas del electrocardiograma.</p>";
	}
	$(".csColl").append(contentShow).trigger('create');
}

/**
 * Mostramos los diagnosticos resultantes a partir de las respuestas
 * obtenidas e incrementamos el n� de procesos realizados
 */
function loadResult () {
	//Validar respuestas y mostrar el resultado del diagn�stico
	showDiags();
	showMed();
	
	if(getVar("pageAnt") && getVar("pageAnt") == "menu"){
		//Si venimos desde la pantalla de menu: ocultar btn anterior
		document.getElementById("divBtnAntQuest").style.display = "none";
		document.getElementById("divBtnAntMeasur").style.display = "none";
		
	} else if (getVar("pageAnt") && getVar("pageAnt") == "questMeasure") {
		//Si venimos desde la pantalla de toma de medidas: ocultar btn anterior
		document.getElementById("divBtnAntQuest").style.display = "none";
		document.getElementById("divBtnAntMeasur").style.display = "block";
		
	} else {
		//Si no: venimos del cuestionario, incrementamos el numero de diagnosticos realizados
		document.getElementById("divBtnAntQuest").style.display = "block";
		document.getElementById("divBtnAntMeasur").style.display = "none";
		incrementResult();		
	}	
	//Mostrar diagnosticos realizados
	showNumDiags();
}

function incrementResult () {
	var contDiags = parseInt(getVar("numDiags"));
	contDiags += 1;
	localStorage.setItem("numDiags",contDiags);
}

function showNumDiags () {
	//Motramos el n�mero de resultados procesados
	var contDiags = parseInt(getVar("numDiags"));
	if (contDiags > 10) {
		getElem("spnNumDiags").style.color = "red";
	}
	getElem("spnNumDiags").innerHTML = contDiags;	
}

function getNameMed (idVarMed) {
	var nameMed = "";
	if (idVarMed) {
		if (idVarMed.indexOf("dur") == 0) {
			nameMed = getNameDuracion(idVarMed);
			
		} else if (idVarMed.indexOf("int") == 0) {
			nameMed = getNameIntensidad(idVarMed);
			
		}
	}
	return nameMed;
}

function getNameDuracion (idVarDur) {
	var nameDur = "";
	if (idVarDur) {
		var arrNameVar = "";
		var dur = "Duraci&oacute;n";
		
		if (idVarDur.indexOf("durOnda") == 0) {
			arrNameVar = idVarDur.split("Onda");
			nameDur = dur+" ONDA "+arrNameVar[1];
		
		} else if (idVarDur.indexOf("durIntr") == 0) {
			arrNameVar = idVarDur.split("Intr");
			nameDur = dur+" Intervalo "+arrNameVar[1];
		
		} else if (idVarDur.indexOf("durComp") == 0) {
			arrNameVar = idVarDur.split("Comp");
			nameDur = dur+" Complejo "+arrNameVar[1];
		
		} else if (idVarDur.indexOf("durSeg") == 0) {
			arrNameVar = idVarDur.split("Seg");
			nameDur = dur+" Segmento "+arrNameVar[1];
		
		}			
	}
	return nameDur;
}

function getNameIntensidad (idVarInt) {
	var nameInt = "";
	if (idVarInt) {
		var arrNameVar = "";
		var int = "Intensidad";
		
		if (idVarInt.indexOf("intOnda") == 0) {
			arrNameVar = idVarInt.split("Onda");
			nameInt = int+" ONDA "+arrNameVar[1];
		
		} else if (idVarInt.indexOf("intSeg") == 0) {
			arrNameVar = idVarInt.split("Seg");
			nameInt = int+" Segmento "+arrNameVar[1];
			
		}
	}
	return nameInt;
}

/******************** Toma de medidas ************************/
function roundThreeDec(num)
{
	var orig = parseFloat(num);
	var result = Math.round(orig*1000)/1000;
	return result;
}

function saveMediciones () {
	var multDur = 0.02;
	var multInt = 0.1;
	
	var arrQuestDur = document.getElementsByName("questDur");
	var arrQuestInt = document.getElementsByName("questInten");
	//Almacenamos valores duracion
	saveValuesMed(arrQuestDur, multDur);
	//Almacenamos valores intensidades
	saveValuesMed(arrQuestInt, multInt);
	
	//redireigimos al resultado
	goToResult("questMeasure");
}

function saveValuesMed (arrQuest, multResult) {
	var quest;
	var questResult;
	
	for (var i in arrQuest) {
		quest = arrQuest[i];
		if (quest != null && quest.id) {
			questResult = roundThreeDec(quest.value * multResult);
			localStorage.setItem(quest.id, questResult);
			saveValuesConcat("respMed", quest.id);			
		}
	}	
}
/**********************************************************/