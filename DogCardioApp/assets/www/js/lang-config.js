String.prototype.printf = function() {
    var formatted = this;
    for(var i=0;i<arguments.length;i++) {
      formatted = formatted.replace("%s", arguments[i]);
    }
    return formatted;
};

var Translation = {
    userLang: 'es',
    getLang: function() {
    	try {	
    		  navigator.globalization.getPreferredLanguage(
    		    function (language) {userLang=language.value;},
    		    function () {alert('Error getting language\n');}
    		  )
    		  alert('language: ' + userLang);
    		} catch (e){
    			
    			var idioma = navigator.language;
    			var arrIdioma = idioma.split("-");
    			userLang = arrIdioma[0];
    			
    		}    	
    },
    strTrans: {
    'en': {
      'spnHelp_preg1' : 'Are defined as QRS complexes those aberrant morphology are very wide and morphology clearly different from that of a normal QRS (sinus).',
      
      'hola': 'hello',
      'tu nombre es %s': 'you name is %s',
      'tienes %s platano' : 'you have %s banana',
      'tienes %s platanos' : 'you have %s bananas',
      }, 
    'es': {
      'spnHelp_preg1' : 'Se definen como complejos QRS de morfolog&iacute;a aberrante a aquellos que son muy anchos y de morfolog&iacute;a claramente diferente a la de un QRS normal (sinusal).',
    	
      'hola': 'hola',
      'tu nombre es %s': 'tu nombre es %s',
      'tienes %s platano' : 'tienes %s platano',
      'tienes %s platanos' : 'tienes %s platanos',
      }, 
    },
    getText: function() {
        var cadena = arguments[0];
        if (typeof this.strTrans[this.userLang] !== "undefined") {
            cadena = this.strTrans[this.userLang][cadena];
        }
       if(typeof cadena == "undefined") cadena = arguments[0];
        
        total = arguments.length;
        for(var i=1;i<total;i++) {
          valor = arguments[i];
          cadena = cadena.replace("%s", arguments[i]);
        }
        return cadena;
    }
};

function setText(idPag){
	Translation.userLang = 'en'; //definimos manualmente el idioma
	var textTrad = Translation.getText("spnHelp_"+idPag);
	getElem("spnHelp_"+idPag).innerHTML = textTrad;
}

function getFrase () {
	Translation.getLang(); //obtenemos el idioma por defecto del navegador
	//Translation.userLang = 'en'; //definimos manualmente el idioma
	alert(Translation.getText('tu nombre es %s','Donkey-Kong'));
	document.write(Translation.getText('tu nombre es %s','Donkey-Kong'));
	document.write('<br />');
	document.write(Translation.getText('tienes %s platano',1));
	document.write('<br />');
	document.write(Translation.getText('tienes %s platanos',4));		
}