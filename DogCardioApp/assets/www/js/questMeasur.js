function roundThreeDec(num)
{
	var orig = parseFloat(num);
	var result = Math.round(orig*1000)/1000;
	return result;
}

function saveMediciones () {
	var multDur = 0.02;
	var multInt = 0.1;
	
	var arrQuestDur = document.getElementsByName("questDur");
	var arrQuestInt = document.getElementsByName("questInten");
	//Almacenamos valores duracion
	saveValuesMed(arrQuestDur, multDur);
	//Almacenamos valores intensidades
	saveValuesMed(arrQuestInt, multInt);
	
	//redireigimos al resultado
	goToResult("questMeasure");
}

function saveValuesMed (arrQuest, multResult) {
	var quest;
	var questResult;
	
	for (var i in arrQuest) {
		quest = arrQuest[i];
		if (quest != null && quest.id) {
			questResult = roundThreeDec(quest.value * multResult);
			localStorage.setItem(quest.id, questResult);
			saveValuesConcat("respMed", quest.id);			
		}
	}	
}