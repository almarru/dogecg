/**
 * jQuery Mobile dynamic popup
 * 
 * Original post: http://ghita.org/jquery/dynamic-mobile-popup
 * Contribute: https://github.com/serbanghita/jquery-plugins
 */
(function($){

    function dynamicPopup(){

        var settings,
            openSettings,
            $wrappers,
            $popup,
            self        = this,
            $activePage = $.mobile.activePage;

        this.init = function(generalOptions, openOptions){

            // Extend the general settings.
            settings = $.extend({
                                    content: '',
                                    popupId: 'popup' + $activePage.attr('id'),
                                    btnAcceptLabel: 'Accept',
                                    btnCancelLabel: 'Cancel',
                                    dialog: false,
                                    themeMain: 'g',
                                    history: false,
                                    callFunctionAccept: false                                
                                }, generalOptions);

            // Extend the popup's open method settings.
            openSettings = $.extend({
                                    positionTo: 'window'
                                }, openOptions);

            if(typeof generalOptions === 'string'){ settings.content = generalOptions; }
            
            self.setPopupWrappers();
            self.popupatePopupContent();
            self.putPopupInDOM();
            self.openPopup();          

            return $popup;

        }

        // Create the popup objects without the actual contents.
        // If the popup container exists return it's objects.
        this.setPopupWrappers = function(){

            $popup = $('#' + settings.popupId);

            // New popup, it doesn't exist.
           if(settings.dialog){ 

                // Create Dialog.
                $wrappers = {
                              main:       $('<div></div>').attr({ 'id': settings.popupId, 'data-role': 'popup', 'data-theme': settings.themeMain, 'data-overlay-theme': 'g' }),
                              content:    $('<div style="text-align:center"></div>').attr({ 'data-role': 'content' }).addClass('ui-content content'),
              				  divLeft:	  $('<div style="widht: 50%; text-align: left; float: left"></div>').addClass('divLeft'),
              				  divRight:	  $('<div style="widht: 50%; text-align: right;"></div>').addClass('divRight'),
//	                          closeX:     $('<a></a>').attr({ 'href': '#', 'data-role': 'button', 'data-rel': 'back', 'data-icon': 'delete', 'data-iconpos': 'notext', 'data-theme': 'b'}).addClass('ui-btn-right closeX').html('Close').button(),
                              btnAccept:   $('<a style="margin-left:5px;margin-top:5px;"></a>').attr({ 'href': '#', 'data-inline': true, 'data-rel': 'back', 'data-icon': 'check', 'data-iconpos': 'left', 'data-theme': 'g', 'data-mini': 'true' }).addClass('btnAccept').html(settings.btnAcceptLabel).button(),
                              btnCancel:   $('<a style="margin-right:5px;margin-top:5px;"></a>').attr({ 'href': '#', 'data-inline': true, 'data-rel': 'back', 'data-icon': 'delete', 'data-iconpos': 'left', 'data-theme': 'g', 'data-mini': 'true' }).addClass('btnCancel').html(settings.btnCancelLabel).button()
                			};

            } else {

                // Create Alert

                $wrappers = {
                				main:       $('<div></div>').attr({ 'id': settings.popupId, 'data-role': 'popup', 'data-theme': settings.themeMain, 'data-overlay-theme': 'g' }),
                				content:    $('<div style="text-align:center"></div>').attr({ 'data-role': 'content' }).addClass('ui-content content'),
                				pcenter: 	$('<p align="center"></p>').addClass('pcenter'),
//                           	closeX:     $('<a></a>').attr({ 'href': '#', 'data-role': 'button', 'data-rel': 'back', 'data-icon': 'delete', 'data-iconpos': 'notext', 'data-theme': 'b'}).addClass('ui-btn-right closeX').html('Close').button(),
                				btnAccept:   $('<a></a>').attr({ 'href': '#', 'data-inline': true, 'data-rel': 'back', 'data-icon': 'check', 'data-iconpos': 'left', 'data-theme': 'g', 'data-mini': 'true' }).addClass('btnAccept').html(settings.btnAcceptLabel).button()
                      		};

            }

            // Apply all possible callback helpers.
            //if(self.settings.helpers){

            //    $.each(self.settings.helpers, function(i, helper){
            //        this.apply(self.el[i]);
            //    });

            //}


        }

        // Populate the popup's content.
        this.popupatePopupContent = function(){

            // 1. Static HTML string.
            if(typeof settings.content === 'string'){
                $wrappers.content.html(settings.content);
            }

            // 2. jQuery object.
            if(settings.content instanceof jQuery){
                // Grab the content inside the wrapper.
                $wrappers.content.html(settings.content.html());
            }

        }

        this.putPopupInDOM = function(){

            // Remove the existing popup from DOM.
            $popup.remove();

            // Tie together the HTML elements.
  //          $wrappers.main.append($wrappers.closeX);
            if (settings.dialog) {
                $wrappers.main.append(($wrappers.content).append(($wrappers.divLeft).append($wrappers.btnAccept)));
            	$wrappers.main.append(($wrappers.content).append(($wrappers.divRight).append($wrappers.btnCancel)));            	
            } else {
                $wrappers.main.append($wrappers.content).append(($wrappers.pcenter).append($wrappers.btnAccept));
           	
            }
            
            // Append the popup to the current page.
            $activePage.append($wrappers.main);
            
        }   

        this.openPopup = function(){

            // Init.
            $popup = $('#' + settings.popupId);
            $popup.popup(settings); 
            $popup.popup({ transition: "pop" });

            if (settings.callFunctionAccept){
	            $wrappers.btnAccept.bind({
	            	click:function(e){
	                    $popup.bind({
	                    	popupafterclose: function(e){
	                    		redirectMenu();
	                    	}
	                    });	            		
	            	}
  
	            });
            } else {
	            $wrappers.btnAccept.bind({
	            	click:function(e){
	                    $popup.bind({
	                    	popupafterclose: function(e){
	                    		$.mobile.changePage('#');
	                    	}
	                    });	            		
	            	}
  
	            });           	
            }
            
            // Open.
            $popup.popup('open', openSettings); 
            
        }             

    }
    
    // Register the plugin.
    $.dynamic_popup = function(generalOptions, openOptions){

        var popup = new dynamicPopup();
        return popup.init(generalOptions, openOptions);

    }

})(jQuery);

     	
